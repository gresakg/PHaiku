<div class="footer-top">
     	  <div class="wrap">
     		<h2><?php echo $form->title; ?></h2>
			<p><?php echo $form->intro; ?></p>
			<div class="section group">
			  <div class="col span_2_of_3">
				  <div class="contact-form">
				  	  <form method="post" action="/#section-5">
					    	<div>
					    		<!-- <input type="text" class="textbox" value="Ime in Priimek (obvezno)" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Ime in priimek';}"> -->
						    	<label for="name"><?php echo $form->name_label; ?></label>
								<input type="text" name="name" value="<?php echo $form->name; ?>" class="">
								<div class="error"><?php echo $errors->name; ?></div>
						    </div>
						    <div>
						    	<!-- <input type="text" class="textbox" value="Email (Obvezno)" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Email';}"> -->
								<label for="eadr"><?php echo $form->email_label; ?></label>
								<input type="text" name="eadr" value="<?php echo $form->eadr; ?>" class="">
								<div class="error"><?php echo $errors->eadr; ?></div>
							</div>
						    <div>
						    	<!-- <textarea value="Sporočilo:" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Sporočilo';}">Čim bolj podrobno opišite vaše želje ...</textarea> -->
								<label for="message"><?php echo $form->message_label; ?></label>
								<textarea name="message" rows="7"><?php echo $form->message; ?></textarea>
								<div class="error"><?php echo $errors->message; ?></div>
							</div>
							<div>
								<label><?php echo $form->recaptcha_label; ?></label>
								<?php echo $form->captcha; ?>
								<div class="error"><?php echo $errors->recaptcha; ?></div>
							</div>
						    <!-- <button class="btn btn-8 btn-8c">Oddaj</button> -->
							<input type="submit" value="Oddaj" class="btn btn-8 btn-8c">
					    </form>
				  </div>
  				</div>
				<div class="col span_1_of_3">
					<div class="company_address">
				     	<h5>Naslov:</h5>
				     
						<ul class="list3">
							<li>
								<img src="<?php echo $site->template_url; ?>/images/location.png" alt=""/>
								<div class="extra-wrap">
								  <p>Med vrtovi 7, 6320 Portorož</p>
								</div>
							</li>
							<li>
								<img src="<?php echo $site->template_url; ?>/images/phone.png" alt=""/>
								<div class="extra-wrap">
									<p>++386 (0)51 341 550</p>
								</div>
							</li>
							<li>
								<img src="<?php echo $site->template_url; ?>/images/mail.png" alt=""/>
								<div class="extra-wrap">
									<p>info (afna) apartma-portoroz.si</p>
								</div>
							</li>
						</ul>
				   </div>
				 </div>
				 <div class="clear"></div>
			  </div>
     	</div>
     </div>

