    <div class="footer-bottom">
       <div class="wrap">
        <div class="copy">
		    <p class="copy">Poganja <a href="http://phaiku.gresak.net">PHaiku</a> </p>
	    </div>
	    <div class="social-footer copy">	
			<p class="copy"><a href="http://gresak.net" target="_blank">gresak.net</a> </p>
	    </div>
	    <div class="clear"></div>
	    <script type="text/javascript">
			$(document).ready(function() {
			
				var defaults = {
		  			containerID: 'toTop', // fading element id
					containerHoverID: 'toTopHover', // fading element hover id
					scrollSpeed: 1200,
					easingType: 'linear' 
		 		};
				
				
				$().UItoTop({ easingType: 'easeOutQuart' });
				
			});
		</script>
           <a href="#" id="toTop" style="display: block;"><span id="toTopHover" style="opacity: 1;"></span></a>
	  </div>
	</div>
	<script src="<?php echo $site->template_url; ?>/js/jquery.scrollTo.js"></script>
	<script src="<?php echo $site->template_url; ?>/js/jquery.nav.js"></script>
	<script>
	$(document).ready(function() {
		$('#nav').onePageNav({
			begin: function() {
			console.log('start')
			},
			end: function() {
			console.log('stop')
			}
		});
	});
	</script>
</div>
<?php echo $widgets->analytics; ?>
</body>
</html>
