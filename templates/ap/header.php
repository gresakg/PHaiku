<!DOCTYPE HTML>
<!--[if lt IE 7 ]><html class="ie ie6" lang="<?php echo $site->language; ?>"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="<?php echo $site->language; ?>"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="<?php echo $site->language; ?>"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="<?php echo $site->language; ?>"> <!--<![endif]-->
<head>
<title><?php echo $site->title; ?></title>
<link href="<?php echo $site->template_url; ?>/css/style.css" rel="stylesheet" type="text/css" media="all" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href='http://fonts.googleapis.com/css?family=Playball&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Open+Sans&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<!--light box-->
<script src="<?php echo $site->template_url; ?>/js/superfish.js"></script>
<script type="text/javascript" src="<?php echo $site->template_url; ?>/js/jquery.lightbox.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo $site->template_url; ?>/css/lightbox.css" media="screen">
	<script type="text/javascript">
		$(function() {
			$('.gallery a').lightBox();
		});
   </script>
<script type="text/javascript" src="<?php echo $site->template_url; ?>/js/move-top.js"></script>
<script type="text/javascript" src="<?php echo $site->template_url; ?>/js/easing.js"></script>
<script type="text/javascript" src="<?php echo $site->template_url; ?>/js/modernizr.custom.min.js"></script> 
<script src="<?php echo $site->template_url; ?>/js/jquery.wmuSlider.js"></script>
<script>
var RecaptchaOptions = {
			theme : 'white'
		};
</script>
<script type="text/javascript">
	 jQuery(document).ready(function($) {
		 $(".scroll").click(function(event){		
			 event.preventDefault();
			 $('html,body').animate({scrollTop:$(this.hash).offset().top},1200);
		 });
		 $('.example1').wmuSlider();
	 });
 </script>
</head>
<body>
   <div class="header">	
    <div class="header-top">
       <div class="wrap"> 
	         <div class="logo">
				<h1><a href="/">Apartma Sabadin Portorož</a></h1>
			 </div>
			 <div class="cssmenu">
				<ul id="nav">
					 <li class="current"><a href="#section-1">Domov</a></li>
					 <li><a href="#section-2">Dobrodošli</a></li>
					 <li><a href="#section-3">Apartmaji</a></li>
					 <li><a href="#section-4">Lokacija</a></li>
				     <li><a href="#section-5">Kontakt</a></li>
				</ul>
		    </div>
		    <div class="clear"></div>
	   </div>
	 </div>
   </div>